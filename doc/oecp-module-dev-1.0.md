# 模块详细设计

### 类图

![输入图片说明](https://cloudmodelingapi.tools.huawei.com/cloudmodelingdrawiosvr/d/svg/12EG)

### 比较类型

|           | 需要提取 | Dumper         | 比较                 | 描述                                                         |
| --------- | -------- | -------------- | -------------------- | ------------------------------------------------------------ |
| repo list | 否       | RepoListDumper | ListCompareExecutor  | 比较DistISO，DistRepo中repo列表差异                          |
| rpm list  | 否       | RPMListDumper  | ListCompareExecutor  | 比较单个repo中rpm包列表差异                                  |
| file list | 否       | FileListDumper | ListCompareExecutor  | 比较单个repo中文件列表差异                                   |
| provides  | 否       | ProvideDumper  | NVSCompareExecutor   | rpm -q --provides的结果                                      |
| requires  | 否       | RequireDumper  | NVSCompareExecutor   | rpm -q --requires的结果                                      |
| config    | 是       | ConfigDumper   | PlainCompareExecutor | 配置文件比较，输出[合并格式的diff](http://www.ruanyifeng.com/blog/2012/08/how_to_read_diff.html]) |
| abi       | 是       | -              | ABIDiff              | 使用abidiff工具带debuginfo比较                               |
| jabi      | 是       | -              | JABIDiff             |                                                              |
| kconfig   | 是       | KConfigDumper  | PlainCompareExecutor | 比较kernel包/boot/config-*文件                               |
| kabi      | 是       | KABIDumper     | ?                    | ~~比较kernel包/boot/symvers-*文件~~                          |
| running   | 是       | RunningDumper  | PlainCompareExecutor | 比较包的可执行--help输出的内容                               |

### 模块

#### 比较器模块

##### 数据结构

TODO !

##### 伪代码

TODO !

#### Dumper模块

##### 数据结构

TODO !

##### Provides

```yam
- rpm: gcc-3.0.4-1
  provides:
  - name: rpmlib(CompressedFileNames)
 	symbol: <=
    version: 3.0.4-1
  - name: cpp
    symbol: <=
    version: 3.0.4-1
- rpm: gcc-common-3.0.4-1
  provides:
  - name: rpmlib(CompressedFileNames)
 	symbol: <=
    version: 3.0.4-1
  - name: cpp
    symbol: <=
    version: 3.0.4-1
```



##### 伪代码

TODO ！

#### 结果模块

##### 数据结构

TODO !

##### 伪代码

TODO ！

#### 主模块

##### 数据结构

###### Repository.rpms

```yaml
- name: gcc
  path: /{work_dir}/x86_64/Packages/gcc-9.3.1-20210204.16.oe1.x86_64.rpm
  verbose_path: gcc-9.3.1-20210204.16.oe1.x86_64.rpm
  raw_path: /{work_dir}/x86_64/Packages/gcc-9.3.1-20210204.16.oe1.x86_64.rpm
  debuginfo_path: /{work_dir}/gcc-debuginfo-9.3.1-20210204.16.oe1.x86_64.rpm
  raw_debuginfo_path: https://repo.openeuler.org/openEuler-21.03/debuginfo/x86_64/Packages/gcc-debuginfo-9.3.1-20210204.16.oe1.x86_64.rpm
```

###### 

###### 包分类Level

[categories](https://gitee.com/openeuler/oec-application/blob/master/doc/compatibility_level/compatibility_level.md#)



##### 比较计划

```
name: all
plan:
- name: repolist
  dumper: RepoListDumper
  executor: ListCompareExecutor
- name: rpmlist
  dumper: RPMListDumper
  executor: ListCompareExecutor
- name: filelist
  dumper: FileListDumper
  executor: ListCompareExecutor
  config:
    cmp_type: 
    strict: True
    short_circut: False
- name: requires
  dumper: RequireDumper
  executor: NVSCompareExecutor
- name: provides
  dumper: ProvidesDumper
  executor: NVSCompareExecutor
- name: running
  dumper: RunningDumper
  executor: PlainCompareExecutor
- name: configuration
  dumper: ConfigDumper
  executor: PlainCompareExecutor
- name: kconfiguration
  dumper: KConfigDumper
  executor: PlainCompareExecutor
  config:
  	package: kernel
- name: kabi
  dumper: KABIDumper
  executor: PlainCompareExecutor
  config:
  	package: kernel
- name: abi
  direct: CompareABI
  config:
  	category: category_level_1
```





##### 伪代码

1.  门禁比较

```yaml
work_dir = "/tmp/compare"
plan_path = "/tmp/plan/default.yaml"

repo_a = Repository("gcc", work_dir)
repo_b = Repository("gcc", work_dir)
repo_a.upsert_a_rpm("/tmp/gcc-7.3.0-20210605.39.oe1.x86_64.rpm", "/tmp/gcc-debuginfo-7.3.0-20210605.39.oe1.x86_64.rpm")
repo_b.upsert_a_rpm("/tmp/gcc-7.3.0-20210606.40.oe1.x86_64.rpm", "/tmp/gcc-debuginfo-7.3.0-20210606.40.oe1.x86_64.rpm")

plan = Plan(plan_path)
result = repo_a.compare(repo_b, plan)
result.export(format="markdown", output="/tmp/compare_sp1_sp2_result.md")
```

2. 比较ISO

```
work_dir = "/tmp/compare"
categories_path = "/tmp/category.yaml"
plan_path = "/tmp/compare_plan"

iso_a = OEDistISO("/tmp/openEuler-20.03-LTS-SP1-x86_64-dvd.iso", work_dir, categories_path)
iso_b = OEDistISO("/tmp/openEuler-20.03-LTS-SP2-x86_64-dvd.iso", work_dir, categories_path)

plan = Plan(plan_path)
result = iso_a.compare(iso_b, plan)
result.export(format="csv", output="/tmp/compare_sp1_sp2_result.csv")
```
