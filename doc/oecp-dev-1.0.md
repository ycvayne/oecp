# 1、需求描述
社区网站发布通过OSV认证的厂商及版本号，对外价值 — 确保根生态一致性，核心特性继承，生态可复用；对内价值 — 反生态分裂识别，保障技术路线演进，OSV认证方案：聚焦openEuler内核和基础包，约束伙伴二次发行版生态核心特性不丢失，关键配置不更改

**Compatible全量换标版本（暂不对外公布）**

在Compatible 深度基础上，软件包版本、特性、配置、补丁全部保持一致，主要更换了发型版版本信息，与openEuler 仓库100%复用。

**Compatible深度：主力行业场景生态复用（对外公布，通用OSV）**

在Compatible 基础基础上，检测生态复用，扩展竞争力特性(etmem、热升级、isulad)继承检测；扩展标准包到L2包一致性，主流行业应用生态复用度90%。检查发行版是否结合社区选包策略及软件包等级策略，检查L1、L2软件包版本、打包方式、接口一致性，实现扩展仓库openEuler系共享、共用。

**Compatible基础：特性/配置继承 (对外公布，企业OSV)**

主要检测openEuler 技术路线，是否有反分裂行为，核心包L1(内核、glibc)及包管理体系一致性，KABI白名单，架构特性(如鲲鹏/X86特性)使能，性能优化配置。检查发行版是否技术路线一致，是否有反分裂行为，核心包一致性，如支持鲲鹏补丁、X86补丁、isulad、热升级等特性合入，充分发挥openEuler竞争力特性。

**检查项**

| 序号 | 检查                   |
| ---- | ---------------------- |
| 1    | 软件包检测             |
| 2    | 接口检测               |
| 3    | 特性检测               |
| 4    | 配置检测               |
| 5    | 补丁检测(需要依赖源码) |

**验证项**

| 序号 | 验证项       | 测试                   |
| ---- | ------------ | ---------------------- |
| 1    | 兼容性测试   | 安装、卸载、命令、服务 |
| 2    | 基础性能测试 | 基础benchmark 测试     |
| 3    | 特性测试     | 特性功能验证           |
| 4    | 功能测试     | 基础AT测试             |

## 1.1、受益人
_围绕特性价值，该特性实现后给哪些角色带来益处，哪些角色会使用_
| 角色 | 角色描述 |
| :--- | :------- |
|      |          |
|      |          |

## 1.3、依赖组件
| 组件                    | 组件描述           | 可获得性                                                     |
| ----------------------- | ------------------ | ------------------------------------------------------------ |
| python3                 | python3.7.9及以上  | 可先通过yum list命令查看，如果没有该版本需要下载安装         |
| sqlite                  | v3.7.17 及以上版本 | 系统自带                                                     |
| abidiff                 | 1.6.0              | 挂载openEuler20.03-LTS-SP2的yum源之后，yum install libabigail |
| japi-compliance-checker | 2.4                | https://github.com/lvc/japi-compliance-checker               |

## 1.3、License

Mulan V2

## 1.4、需求清单

### 1.4.1、工具需求规格项

| 任务项            | 内容                       | 责任人         | 状态  | 计划完成时间 |
| ----------------- | :------------------------- | -------------- | ----- | ------------ |
| 认证整体介绍材料  | OSV 认证整体思路及路标规划 | 杜开田         | open  | 2021/9/5     |
|                   | 认证流程建立               | 杜开田         | open  | 2021/12/30   |
| 认证文档发布      | 软件包兼容性等级及清单定义 | 杜开田         | close | 2021/7/30    |
|                   | KABI接口清单               | 谢秀奇         | close | 2021/7/30    |
|                   | 与换标相关的软件包清单     | 杜开田         | open  | 2021/9/3     |
|                   | 兼容性等级/API变更管理规范 | 杜开田、谢秀奇 | open  | 2021/10/30   |
| OSV认证需求规格   | 需求分析说明书             | 杜开田         | open  | 2021/9/3     |
|                   | 需求规格分解分配           |                | open  | 2021/8/29    |
| OSV系统设计说明书 | OSV系统设计说明书          |                | open  | 2021/8/29    |

### 1.4.2、工具检测能力项

| 任务项                      | 内容                                                         | 责任人 | 状态 | 计划完成时间 |
| --------------------------- | ------------------------------------------------------------ | ------ | ---- | ------------ |
| 软件包管理机制检测          | 包provides 一致性检测 | 孙李军 | close | 2021/9/30    |
|                             | 依赖包检测，含依赖版本信息检测，不包括release版本，到第一层依赖即可 |    孙李军    | close| 2021/9/30    |
| 单软件包差异检测            | 软件包版本(区分大小版本，release版本)、名称一致性检测        |孙李军        | close | 2021/9/30    |
|                             | 软件包静态文件存在一致性检测，包括静态库、动态库、二进制、头文件、配置文件 |  孙李军      | close | 2021/9/30    |
|                             | 软件包静态文件所在目录结构一致性检测，包括静态库、动态库、二进制、头文件、配置文件 |   孙李军     | close | 2021/9/30    |
|                             | 软件包静态文件修订检测，包括头文件、配置文件，增加CRC校验    |        |      | 2021/12/30   |
| 软件包等级管理              | L1 软件包检测，基础检测项                                    |    邓鹏    |    close  | 2021/9/30    |
|                             | L2 软件包检测，差异化检测项                                  |     邓鹏   |  close    | 2021/9/30    |
|                             | 换标软件包信息检测，基本检测项                               |     邓鹏   |   close   | 2021/9/30    |
|                             | 支持L1、L2、换标清单更新，在设计阶段 需要保持解耦            |        |      | 2021/12/30   |
| 函数接口级差异检测          | C/C++/java库文件对外函数接口检查                             |  孙李军      |  close    | 2021/9/30    |
|                             | 数据结构接口检查                                             | 孙李军       | close     | 2021/9/30    |
|                             | 消息通讯接口检查？                                           |        |      | 2021/12/30   |
|                             | 内核KABI接口一致性检查                                       | 曹学亮 |close | 2021/9/30    |
|                             | 面向glibc 提供系统调用接口一致性检查                         |        |      | 2021/12/30   |
| 配置文件差异检测            | 内核启动差异检查                                             |        |      | 2021/12/30   |
|                             | 内核选项配置参数差异检查                                     | 曹学亮 |open  | 2021/12/30   |
|                             | 系统配置参数默认参数差异，基于sysctl                         |        |      | 2021/12/30   |
| 系统命令及服务差异检测      | 系统服务检查，关注系统服务的启动参数                         |        |      | 2021/9/30    |
|                             | Linux 命令检查，主要环境变量取bin目录(包括bin/sbin) help命令差异及version 比较 |        |      | 2021/9/30    |
| 支持compatible  结果检测    | 基于L1/L2/软件包，及内核KABI一致性检测                       | 鲁伟涛 |close | 2021/9/30    |
|                             | 对于选择的包，命令、服务、接口一致性检测                     | 鲁伟涛 |close | 2021/9/30    |
|                             | 基于换标版本信息检测                                         | 鲁伟涛 |close | 2021/9/30    |
| 支持openEuler  基础版本检测 | 支持openEuler 基础版本检测，基于软件包版本                   |        |      | 2021/12/30   |
|                             | 支持基于openEuler 20.03 sp1 版本检测                         | 鲁伟涛 |close | 2021/9/30    |
|                             | 支持基于openEuler 20.03   版本检测                           | 鲁伟涛 |close | 2021/9/30    |
| 支持validate  结果检测      | 基于L1软件包，及内核KABI一致性检测                           |        |      | 2021/12/30   |
|                             | 对于选择的包，配置、命令、服务、接口一致性检测               |        |      | 2021/12/30   |
|                             | 基于换标版本检测                                             |        |      | 2021/12/30   |
| 支持换标结果检测            | 基于换标版本检测                                             | 鲁伟涛 |colse | 2021/9/30    |
|                             | 基于全量包，配置、命令、服务、接口一致性检测，全量包版本相似度95%以上为换标 |鲁伟涛 | colse | 2021/9/30    |

### 1.4.3、工具展示项

| 任务项   | 内容                              | 责任人 | 状态 | 计划完成时间 |
| -------- | --------------------------------- | ------ | ---- | ------------ |
| 结果展示 | compatible  结果检查报告，CSV报告 |鲁伟涛  | close| 2021/9/30    |
|          | validate 结果检查报告             |        |      | 2021/12/30   |
|          | 换标结果 结果检查报告，CSV报告    |鲁伟涛  | close| 2021/9/30    |

### 1.4.4、工具验证用例项

| 任务项                     | 内容                                                         | 责任人 | 状态 | 计划完成时间 |
| -------------------------- | ------------------------------------------------------------ | ------ | ---- | ------------ |
| 版本功能级测试用例集成     | 封装AT用例                                                   |        |      | 2021/12/30   |
| 版本特性级测试用例集成     | 鲲鹏补丁，如：内核热升级、etmem等特性                        |        |      | 2021/12/30   |
| 版本性能级测试用例集成     | 支持unixbench 性能集成，性能不低于基线值，XXX                |  刘隐四 | open | 2021/10/15    |
|                            | 支持lmbench  性能集成，性能不低于基线值，XXX                 |  刘隐四 | open | 2021/10/15    |
|                            | 支持mysql  性能测试，性能指标不低于极限值，                  | 刘隐四  | open | 2021/10/15    |
|                            | 检测当前硬件配置环境                                         |        |      |              |
| 版本性仓库共用测试用例集成 | 支持基于原始版本仓库自动挂载                                 |        |      | 2021/12/30   |
|                            | 支持仓库软件包，安装、卸载、命令调用、服务启停遍历验证，软件包安装成功率与openEuler持平 | 曹学亮|  open  | 2021/9/30    |

# 2、设计概述

## 2.1、分析思路

## 2.2、设计原则

- 数据与代码分离： 功能实现是需要考虑哪些数据是需要配置动态改变的，不可在代码中将数据写死，应提取出来做成可配置项。

- 分层原则： 上层业务模块的数据查询应通过查询模块调用数据库获取数据，不可直接跨层访问数据库。

- 接口与实现分离：外部依赖模块接口而不是依赖模块实现。

- 模块划分： 模块之间只能单向调用，不能存在循环依赖。

- 变更： 架构设计变更、外部接口变更、特性需求变更需要结合开发、测试等所有相关人员经过开会讨论后决策，不可擅自变更。

# 3、需求分析
## 3.1、USE-CASE视图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0924/171014_169afbed_6503854.jpeg "com.jpg")

## 3.2、工具上下文


![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/193011_5f76b9dd_6503854.jpeg "工具上下文.jpg")
## 3.3、逻辑视图


![输入图片说明](https://images.gitee.com/uploads/images/2021/0923/192735_2ec6f370_6503854.jpeg "逻辑模型.jpg")



## 3.4、开发视图


| 主目录      | 二级目录               | 三级目录                | 描述                                   |
| ----------- | ---------------------- | ----------------------- | -------------------------------------- |
| cli.py      |                        |                         | 命令启动脚本                           |
| requirement |                        |                         | 工具依赖清单                           |
| README.md   |                        |                         | 用户指导手册                           |
| test        |                        |                         | 测试脚本夹                             |
| doc         |                        |                         | 设计文档文件夹                         |
|             | oecpimg                |                         | 存放文档图片                           |
|             | oecp-dev-1.0.md        |                         | 设计文档                               |
|             | oecp-module-dev-1.0.md |                         | 模块设计文档                           |
| oecp        |                        |                         |                                        |
|             | main                   |                         | 主模块                                 |
|             |                        | factory.py              | 工厂方法，生产ISO、REPO等比较对象      |
|             |                        | directory.py            | 目录级别对象、ISO对象、REPO对象        |
|             |                        | repository.py           | 仓库级别对象                           |
|             |                        | mapping                 | 二进制包和源码包映射                   |
|             |                        | category.py             | 软件包、二进制包等级                   |
|             |                        | plan.py                 | 比较计划                               |
|             | executor               |                         | 比较模块                               |
|             |                        | base.py                 | 比较器基类                             |
|             |                        | abi.py                  | 比较abi                                |
|             |                        | jabi.py                 | 比较jar abi                            |
|             |                        | list.py                 | 比较文件列表、包列表                   |
|             |                        | null.py                 | 空比较，当比较计划项只需要dumper时使用 |
|             |                        | nvs.py                  | 符号、版本、名称比较器                 |
|             |                        | plain.py                | 文件比较                               |
|             | dumper                 |                         | dumper模块                             |
|             |                        | base.py                 | dumper基类                             |
|             |                        | abi.py                  | 动态库abi                              |
|             |                        | config.py               | rpm包的配置文件                        |
|             |                        | extract.py              | 提取rpm包内容                          |
|             |                        | filelist.py             | 文件列表                               |
|             |                        | jabi.py                 | jar包abi                               |
|             |                        | kabi.py                 | 内核abi                                |
|             |                        | kconfig.py              | 内核配置                               |
|             |                        | null.py                 | 当比较计划项只需要执行比较时使用       |
|             |                        | packagelist.py          | ISO中包列表                            |
|             |                        | provides.py             | rpm包提供的符号                        |
|             |                        | requires.py             | rpm包依赖的符号                        |
|             | result                 |                         | 结果模块                               |
|             |                        | compare_result.py       | 保存结果对象                           |
|             |                        | constants.py            | 比较类型、比较结果宏                   |
|             |                        | export.py               | 导出比较结果到csv文件                  |
|             |                        | test_result.py          | 导出compass-ci比较的结果               |
|             | proxy                  |                         | 第三方代理模块                         |
|             |                        | rpm_proxy.py            | rpm包常用方法                          |
|             |                        | proxy/requests_proxy.py | requests功能封装下载功能               |
|             | utils                  |                         | 工具模块                               |
|             |                        | utils/logger.py         | 日志                                   |
|             |                        | utils/misc.py           | 常用工具                               |
|             |                        | utils/shell.py          | shell命令                              |
|             |                        | utils/unit_convert.py   | 单位转换                               |
|             | conf                   |                         | 配置模块                               |
|             |                        | category                | 包等级配置                             |
|             |                        | performance             | compass-ci性能测试                     |
|             |                        | plan                    | 比较计划                               |
|             |                        | logger.conf             | 日志配置                               |

## 3.5、部署视图
_真实环境如何部署，网络和存储如何划分，业务程序如何部署，如何扩展、备份等_

## 3.6、质量属性设计
### 3.6.1、性能规格
在运行oecp分析之前，确保虚拟机或物理机的运行规格**大于等于2U8G**，运行前建议重启虚拟机，保证空闲内存足够，建议oecp所在目录空闲磁盘至少**保留20GB**（具体以实际扫描rpm包数量为准）

| 任务分析项                  | CPU资源消耗      | 运行耗时  | 输出件大小                                           |
| --------------------------- | ---------------- | --------- | ---------------------------------------------------- |
| 比较两个iso镜像所有比较计划 | 2核心，峰值100%  | 20~60分钟 | 程序正常结束，百兆级别，具体大小和比较的镜像差异有关 |
| 比较两个iso镜像所有比较计划 | 16核心，峰值100% | 10~60分钟 | 程序正常结束，百兆级别，具体大小和比较的镜像差异有关 |

### 3.6.2、系统可靠性设计

### 3.6.3、安全性设计

### 3.6.4、兼容性设计

### 3.6.5、可服务性设计

### 3.6.6、可测试性设计

## 3.7、特性清单
| no   | 特性描述 | 代码估计规模 | 实现版本 |
| :--- | :------- | :----------- | :------- |
|      |          |              |          |
|      |          |              |          |

## 3.8、接口清单
### 3.8.1、命令行接口清单


`python3 cli.py [-h] [-n PARALLEL] [-w WORK_DIR] [-p PLAN_PATH]
                [-c CATEGORY_PATH] [-b PERF_BASELINE_FILE] [-a {x86_64,aarch64}]
                [-f OUTPUT_FORMAT] [-o OUTPUT_FILE]
                file file`
* **位置参数**
  * **`file`**
    指定两个比较的iso文件

* **可选参数**

  * **`-n, --parallel`**
    指定`进程池并发数量`，默认cpu核数

  * **`-w, --work-dir`**
    指定`工作路径`，默认路径为/tmp/oecp
  
  * **`-p, --plan`**
    指定`比较计划`

  * **`-c, --category`**
    指定`包级别信息`，默认为src/conf/category/category.json

  * **`-b, --baseline`**
    指定`基线文件`，默认为src/conf/performance/openEuler-20.03-LTS-aarch64-dvd.iso.performance.json

  * **`-a, --arch`**
    指定`架构`，从x86_64和aarch64中选择一个，默认为x86_64

  * **`-f, --format`**
    指定`输出格式`，默认为csv

  * **`-o, --output`**
    指定`输出结果路径`，默认为/tmp/oecp
    

* **举例**

  * ` python3 cli.py /root/openEuler-20.03-LTS-aarch64-dvd.iso /root/openEuler-20.03-LTS-SP1-aarch64-dvd.iso -p src/conf/plan/test.json`

### 3.8.2、内部模块间接口清单
| 接口名称                                         | 接口描述                                                     | 入参                                                         | 输出                                                         | 异常                                                         |
| :----------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| `Repository.upsert_a_rpm`                        | 增加一个rpm包                                                | rpm路径、rpm文件名、debuginfo包路径                          |                                                              | 无                                                           |
| `Repository.compare`                             | 比较repository                                               | 比较对手、比较计划                                           | 比较结果对象                                                 | 无                                                           |
| `Repository.\__getitem__`                        | 支持遍历所有rpm包                                            |                                                              | 每一个rpm包描述                                              | 无                                                           |
| `Directory.upsert_a_group`                       | 增加一个子目录                                               | 子目录路径、debuginfo路径、sqlite文件路径                    |                                                              | 无                                                           |
| `Directory.compare`                              | 比较目录                                                     | 比较对手、比较计划                                           | 比较结果对象                                                 | 无                                                           |
| `DistISO.compare`                                | 比较发布的ISO                                                | 比较对手、比较计划                                           | 比较结果对象                                                 | 无                                                           |
| `OBSRepo`                                        | OBS内部publish的repo                                         |                                                              |                                                              |                                                              |
| `Plan.dumper_of`                                 | 比较项调用的dumper                                           | 比较项名称                                                   | dumper类对象                                                 | 无                                                           |
| `Plan.executor_of`                               | 比较项调用的比较器                                           | 比较项名称                                                   | 比较器类对象                                                 | 无                                                           |
| `Plan.config_of`                                 | 比较项相关的配置                                             | 比较项名称                                                   | 比较项配置字典                                               | 无                                                           |
| `Plan.only_for_directory`                        | 比较项只针对目录级别对象有效                                 | 比较项名称                                                   | boolean                                                      | 无                                                           |
| `Plan.check_specific_package`                    | 比较项只针对特定的包                                         | 比较项名称、包名                                             | boolean                                                      | 无                                                           |
| `Plan.check_specific_category`                   | 比较项只针对特定分类的包                                     | 比较项名称、分类级别                                         | boolean                                                      | 无                                                           |
| `CategoryLevel.level_name_2_enum`                | 包分类级别转换成枚举属性                                     | 包分类级别名称                                               | 包分类级别枚举属性                                           | 无                                                           |
| `Category.category_of_src_package`               | 获取代码包分类级别                                           | 包名称                                                       | 分类级别                                                     | 无                                                           |
| `Category.category_of_bin_package`               | 获取二进制包分类级别                                         | 包名称                                                       | 分类级别                                                     | 无                                                           |
| `RepositoryPackageMapping.repository_of_package` | 二进制包对应的代码包                                         | 二进制包名称                                                 | 代码包名称                                                   | 无                                                           |
| `SQLiteMapping.repository_of_package`            | 根据sqlite获取二进制包对应的代码包                           | 二进制包名称                                                 | 代码包名称                                                   | 无                                                           |
| `compare_result`                                 | 解析result树中的比较结果，<br>输出scv格式的报告              | result树，例如：<br>{<br> &nbsp;&nbsp;"cmp_side_a": "openeEuler-20.03-LTS-aarch64-dvd.iso",<br>&nbsp;&nbsp;"cmp_side_b": "openeEuler-20.03-LTS-SP1-aarch64-dvd.iso",<br>&nbsp;&nbsp;"cmp_type": null, <br>&nbsp;&nbsp;"cmp_result": "diff",<br>&nbsp;&nbsp;"diff_components":[<br>&nbsp;&nbsp;&nbsp;&nbsp; repository_result_1,<br>&nbsp;&nbsp;&nbsp;&nbsp; repository_result_2, <br>&nbsp;&nbsp;&nbsp;&nbsp; ...<br>&nbsp;&nbsp;]<br>} | work-dir/report: <br>&nbsp;&nbsp; all-rpm-report.csv, <br>&nbsp;&nbsp; rpm-\*/\*.csv |                                                              |
| `test_result`                                    | 解析compass-ci测试结果，<br>输出对应csv格式报告              | compass-ci性能测试结果， 如：<br>&nbsp;&nbsp; work-dir/openeEuler-20.03-LTS-aarch64-dvd.iso.performance.json <br>&nbsp;&nbsp; work-dir/openeEuler-20.03-LTS-SP1-aarch64-dvd.iso.performance.json<br> compass-ci服务命令启停测试结果，如：<br>&nbsp;&nbsp; work-dir/openeEuler-20.03-LTS-aarch64-dvd.iso.tests.json <br>&nbsp;&nbsp; work-dir/openeEuler-20.03-LTS-aarch64-dvd.iso.tests.json | work-dir/report: <br>&nbsp;&nbsp; all-performance-report.csv， <br>&nbsp;&nbsp; rpm-tests/\*.csv | work-dir/openeEuler-20.03-LTS-aarch64-dvd.iso.tests.json not exists,<br> work-dir/openeEuler-20.03-LTS-SP1-aarch64-dvd.iso.tests.json not exists,<br> work-dir/openeEuler-20.03-LTS-SP1-aarch64-dvd.iso.performance.json not exists,<br> work-dir/openeEuler-20.03-LTS-SP1-aarch64-dvd.iso.performance.json not exists |
| `RPMExtractDumper`                               | rpm提取的dumper，一次提取，多次使用                          | repository,cache,config                                      | rpm解压提取的内容                                            |                                                              |
| `ABIDumper`                                      | 依赖RPMExtractDumper接口，从rpm解压dumper中提取so库文件      | repository,cahce,config                                      | so库文件字典封装                                             |                                                              |
| `ConfigDumper`                                   | 依赖RPMExtractDumper，从rpm解压dumper中提取配置文件          | repository,cache,config                                      | 配置文件的字典封装                                           |                                                              |
| `JABIDumper`                                     | 依赖RPMExtractDumper，从rpm解压dumper中提取jar包             | repository,cache,config                                      | jar包文件字典的封装                                          |                                                              |
| `FileListDumper`                                 | 获取rpm包内文件列表                                          | repository,cache,config                                      | rpm包内文件列表的字典封装                                    |                                                              |
| `PackageListDumper`                              | 获取仓库目录rpm包列表                                        | directory,config                                             | 仓库目录所有rpm列表的字典封装                                |                                                              |
| `ProvidesDumper`                                 | 获取rpm的provides                                            | repository,cache,config                                      | rpm的provides列表的字典封装                                  |                                                              |
| `RequiresDumper`                                 | 获取rpm的requires                                            | repository,cache,config                                      | rpm的requires列表的字典封装                                  |                                                              |
| `KabiDumper`                                     | 依赖RPMExtractDumper，解压kernel rpm后从symvers gz中提取kabi文件，经过白名单过滤 | repository,cache,config                                      | 内核接口内容的封装成NVSCompareExecutor可处理的对象           |                                                              |
| `KconfigDumper`                                  | 依赖RPMExtractDumper，解压kernel rpm后从config文件中提取编译配置文件 | repository,cache,config                                      | 内核编译配置内容封装成NVSCompareExecutor可处理的对象         |                                                              |
| `ABICompareExecutor`                             | 比较两个abi的dumper                                          | dump_a,dump_b,config                                         | abi比较结果对象树和差异保存到文件                            |                                                              |
| `JABICompareExecutor`                            | 比较两个jabi的dumper                                         | dump_a,dump_b,config                                         | jabi的比较结果对象树和差异保存到文件                         |                                                              |
| `ListCompareExecutor`                            | 支持FileListDumper和PackageListDumper对象的dumper比较        | dump_a,dump_b,config                                         | 比较结果对象树                                               |                                                              |
| `NVSCompareExecutor`                             | 比较组件name/version/symbol的dumper                          | dump_a,dump_b,config                                         | 比较结果对象树                                               |                                                              |
| `PlainCompareExecutor`                           | 比较conifg配置文件内容                                       | dump_a,dump_b,config                                         | 比较结果对象树和diff的文件内容                               |                                                              |




# 4、修改日志

| 版本 | 发布说明 |
| :--- | :------- |
|      |          |
|      |          |


# 5、参考目录
