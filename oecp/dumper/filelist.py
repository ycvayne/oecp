import logging
import os

from oecp.dumper.base import AbstractDumper
from oecp.utils.shell import shell_cmd

logger = logging.getLogger('oecp')


class FileListDumper(AbstractDumper):

    def __init__(self, repository, cache=None, config=None):
        super(FileListDumper, self).__init__(repository, cache, config)
        self._cmd = ['rpm', '-pql']

    def dump(self, repository):
        rpm_path = repository['path']
        dump_list = []
        cmd = self._cmd
        if not cmd:
            raise ValueError('%s should be command list' % cmd)
        cmd = cmd + [rpm_path]
        code, out, err = shell_cmd(cmd)
        if not code:
            if err:
                logger.warning(err)
            if out:
                for line in out.split("\n"):
                    if not line:
                        continue
                    dump_list.append(line)
        item = {'rpm': os.path.basename(rpm_path), 'kind': 'filelist', 'data': dump_list}
        item.setdefault('category', repository['category'].value)
        return item

    def run(self):
        dumper_list = []
        for _, repository in self.repository.items():
            dumper = self.dump(repository)
            dumper_list.append(dumper)
        return dumper_list
