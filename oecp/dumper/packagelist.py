import logging

logger = logging.getLogger('oecp')


class PackageListDumper:

    def __init__(self, directory, config=None):
        self.config = config if config else {}
        self.directory = directory
        self.data = 'data'

    def dump(self):
        item = {}
        for repo, repo_item in self.directory.items():
            for rpm_name in repo_item:
                rpm = repo_item[rpm_name]['verbose_path']
                category = repo_item[rpm_name]['category'].value
                source_package = repo_item.verbose_path
                attr = {'category': category, 'source_package': source_package}
                item.setdefault(rpm, attr)
        result = {'path': self.directory.verbose_path, self.data: item}
        return result

    def run(self):
        result = self.dump()
        return result
