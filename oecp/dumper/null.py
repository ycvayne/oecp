from oecp.dumper.base import AbstractDumper


class NullDumper(AbstractDumper):

    def __init__(self, repository, cache=None, config=None):
        super(NullDumper, self).__init__(repository, cache, config)

    def run(self):
        return self.repository
