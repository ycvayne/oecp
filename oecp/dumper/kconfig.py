import os
import re
import logging

from oecp.dumper.base import AbstractDumper
from oecp.utils.kernel import get_file_by_pattern


class KconfigDumper(AbstractDumper):
    def __init__(self, repository, cache=None, config=None):
        super(KconfigDumper, self).__init__(repository, cache, config)
        cache_require_key = 'extract'
        self.cache_dumper = self.get_cache_dumper(cache_require_key)
        self._component_key = 'kconfig'
        self.data = "data"

    def load_kconfig(self):
        kconfig = get_file_by_pattern(r"^config", self.cache_dumper)
        item = {}
        kernel = 'kernel'
        if 'kernel-core' in kconfig:
            kernel = 'kernel-core'

        item.setdefault('rpm', self.repository.get(kernel).get('verbose_path'))
        item.setdefault('kind', self._component_key)
        item.setdefault('category', self.repository.get(kernel).get('category').value)
        with open(kconfig, "r") as f:
            for line in f.readlines():
                line = line.strip().replace("\n", "")
                if line == "":
                    continue
                if line.startswith("#"):
                    continue

                name, version = line.split("=", 1)
                item.setdefault(self.data, []).append({'name': name, 'symbol': "=" , 'version': version})
        return [item]

    def run(self):
        return self.load_kconfig()


