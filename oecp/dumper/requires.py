from oecp.dumper.base import ComponentsDumper


class RequiresDumper(ComponentsDumper):

    def __init__(self, repository, cache=None, config=None):
        super(RequiresDumper, self).__init__(repository, cache, config)
        self._cmd = ['rpm', '-pq', '--requires', '--nosignature']
        self._component_key = 'requires'
