from oecp.dumper.base import ComponentsDumper


class ProvidesDumper(ComponentsDumper):

    def __init__(self, repository, cache=None, config=None):
        super(ProvidesDumper, self).__init__(repository, cache, config)
        self._cmd = ['rpm', '-pq', '--provides', '--nosignature']
        self._component_key = 'provides'
