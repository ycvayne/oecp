# -*- encoding=utf-8 -*-
"""
# **********************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# [oecp] is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#     http://license.coscl.org.cn/MulanPSL
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v1 for more details.
# Author:
# Create: 2021-09-08
# Description: compare result
# **********************************************************************************
"""

import csv
import os
import json
import logging
from oecp.result.constants import *
from oecp.proxy.rpm_proxy import RPMProxy
from oecp.main.category import *

logger = logging.getLogger("oecp")


# ------------------------------------------------------------------------------------
# parse compass-ci test result for export csv report
# requires:
#   - performacne: 2 performace test result at least in root_path(the result output path)
#     eg:
#        openEuler-20.03-LTS-aarch64-dvd.iso.performance.json
#        openEuler-20.03-LTS-SP1-aarch64-dvd.iso.performance.json
#
#   - rpm tests: 2 cmds+services test result at least in root_path
#     eg:
#        openEuler-20.03-LTS-aarch64-dvd.iso.tests.json
#        openEuler-20.03-LTS-SP1-aarch64-dvd.iso.tests.json
# ------------------------------------------------------------------------------------

def performance_result_parser(side_a, side_b, root_path, baseline):
    perf_result = []
    side_a_result = get_performacnce_result(side_a, root_path)
    side_b_result = get_performacnce_result(side_b, root_path)
    base_line_result = load_json_result(baseline)
    if not (base_line_result and side_b_result and side_a_result):
        return perf_result

    for metric, value in base_line_result.items():
        avg_b = side_b_result[metric]['average']
        baseline_avg = value['average']
        cmp_result = 'fail' if avg_b - baseline_avg < 0 else 'pass'
        row = {
            "metric": metric,
            side_a: side_a_result[metric]['average'],
            side_b: avg_b,
            "baseline": baseline_avg,
            "compare result": cmp_result
        }
        perf_result.append(row)

    return perf_result

def get_performacnce_result(side, root_path):
    # find the result of side in root_path
    result_path = root_path + side + '.performance.json'

    return load_json_result(result_path)

# ------------------------------------------------------------------------
# parse the test result which includes: rpm-cmds and rpm service
def test_result_parser(side_a, side_b, root_path):
    dump_a = get_test_result(side_a, root_path)
    dump_b = get_test_result(side_b, root_path)

    if dump_b and dump_a:
        category_map = create_category_map()
        return assain_rpm_test_result(side_a, side_b, dump_a, dump_b, category_map)
    else:
        return [], {}

def assgin_rpm_summay(rpm_test_details, side_a, side_b):
    summary = {}
    for rpm, rpm_result in rpm_test_details.items():
        values = rpm_result[CMP_TYPE_RPMS_TEST]
        rpm_side_result = {
                side_a: "pass",
                side_b: "pass"
                }
        for v in values:
            level = v.get("category level") if v.get("category level") else "6"
            summary.setdefault(level, {
                "category level": level,
                "[success] " + side_a: 0,
                "[fail] " + side_a: 0,
                "[success] " + side_b: 0,
                "[fail] " + side_b: 0,
                })
            for side in [side_a, side_b]:
                if v[side].endswith("fail"):
                    rpm_side_result[side] = "fail"
        for s, result in rpm_side_result.items():
            if result == "fail":
                summary[level]["[fail] " + s] += 1
            else:
                summary[level]["[success] " + s] += 1
    return sorted(summary.values(), key = lambda i: i["category level"])

def assain_rpm_test_result(side_a, side_b, dump_a, dump_b, category_map):
    rpm_test_rows = []
    rpm_test_details = {}
    a_keys = dump_a.keys()
    b_keys = dump_b.keys()
    keys = list(set(a_keys).union(set(b_keys)))
    for key in keys:
        va = dump_a.get(key)
        vb = dump_b.get(key)
        category = category_map.get(key, '')

        rpm_cmp_result = compare(va, vb)
        rpm_pkg = get_category(key, category, 'bin')
        rpm_side_a = key if key in a_keys else ''
        rpm_side_b = key if key in b_keys else ''
        source_pkg = get_category(key, category, 'src')
        category_level = get_category(key, category, 'level')
        rpm_test_details.setdefault(rpm_pkg, {})
        rpm_test_details[rpm_pkg][CMP_TYPE_RPMS_TEST] = assain_rpm_test_details(rpm_pkg, side_a, side_b, va, vb, category_level)

        row = {
            side_a + " binary rpm package": rpm_side_a,
            side_a + " source package": source_pkg,
            side_b + " binary rpm package": rpm_side_b,
            side_b + " source package": source_pkg,
            "compare result": rpm_cmp_result,
            "compare detail": ' ' + CMP_TYPE_RPMS_TEST.replace(' ', '-') + '/' + key + '.csv ',
            "compare type": CMP_TYPE_RPMS_TEST,
            "category level": category_level
        }
        rpm_test_rows.append(row)

    return rpm_test_rows, rpm_test_details

def assain_rpm_test_details(rpm_pkg, side_a, side_b, dump_a, dump_b, level):
    rows = []
    dump_a = format_test_detail(dump_a)
    dump_b = format_test_detail(dump_b)

    a_keys = dump_a.keys() if dump_a else ''
    b_keys = dump_b.keys() if dump_b else ''
    keys = list(set(a_keys).union(set(b_keys)))

    for key in keys:
        va = key + ': ' + dump_a.get(key) if dump_a and dump_a.get(key) else ''
        vb = key + ': ' + dump_b.get(key) if dump_b and dump_b.get(key) else ''
        row = {
            "binary rpm package": rpm_pkg,
            side_a: va,
            side_b: vb,
            "compare result": compare(va, vb),
            "compare type": CMP_TYPE_RPMS_TEST,
            "category level": level
        }
        rows.append(row)
    return rows

def format_test_detail(result):
    '''
    :param: result
        eg: {
                "install": "pass"
	        "cmds": {
                    "/usr/bin/dockerd": "pass",
                    "/usr/bin/runc": "pass",
                    "/usr/bin/docker": "pass"
                },
		...
	    }
    '''
    if not result:
        return ''
    new_result = {}
    for k, v in result.items():
        parse_dump(k, v, new_result)
    return new_result

def parse_dump(key, value, new_result):
    if isinstance(value, dict):
        for k, v in value.items():
            parse_dump(key + '.' + k, v, new_result)
    else:
        new_result[key] = value

def compare(a, b):
    if a == b:
        return CMP_RESULT_SAME
    elif a and (not b):
        return CMP_RESULT_LESS
    elif b and (not a):
        return CMP_RESULT_MORE
    else:
        return CMP_RESULT_DIFF

def get_category(name, category, category_type):
    '''
    :param: type
        eg: level | bin | src
    '''
    if not category:
        if category_type == 'level':
            return ''
        return name
    else:
        if category_type == 'level':
            level_num = category.get(category_type, '')
            return level_num[-1] if level_num else level_num
        return category.get(category_type, name)

def get_test_result(side, root_path):
    result_path = root_path + side + '.tests.json'

    return load_json_result(result_path)

def load_json_result(result_path):
    try:
        if not os.path.exists(result_path):
            logger.warning(f"{result_path} not exist")
            return None
        with open(result_path, "r") as f:
            return json.load(f)
    except FileNotFoundError:
        logger.warning(f"{os.path.realpath(result_path)} not exist")
        return None

def create_category_map():
    category_map = {}
    category_file = os.path.join(os.path.dirname(__file__), "../conf/category/category.json")
    categorys = load_json_result(category_file)
    for category in categorys:
        #name = RPMProxy.rpm_n_v_r_d_a(category["bin"], dist="category")[0]
        try:
            name = RPMProxy.rpm_name(category["bin"])
            category_map[name] = category
        except AttributeError as e:
            logger.warning(f"\"{category['oecp']}\" or \"{category['bin']}\" is illegal rpm name")
    return category_map
