# -*- encoding=utf-8 -*-
"""
# **********************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# [oecp] is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#     http://license.coscl.org.cn/MulanPSL
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v1 for more details.
# Author:
# Create: 2021-09-08
# Description: compare result
# **********************************************************************************
"""

import csv
import os

def create_csv_report(header, rows, report_path):
    if report_path:
        with open(report_path, 'w', newline = '', encoding='utf-8') as f:
            f_csv = csv.DictWriter(f, header)
            f_csv.writeheader()
            f_csv.writerows(rows)
    else:
        return None

def create_directory(root_path, report_name, osv_title, cmp_type = None, uid=''):
    """
    directory structure
    - reports
        - rpm-provides
            - xxx.rpm_provides.csv
        - rpm-requires
            - xxx.rpm_requires.csv
        ...
    """
    if cmp_type:
        if os.path.exists(root_path):
            first_path = root_path + '/' + osv_title
            if not os.path.exists(first_path):
                os.makedirs(first_path)

            second_path = first_path + '/' + cmp_type.replace(' ', '-')
            if not os.path.exists(second_path):
                os.makedirs(second_path)

            report_path = second_path + '/' + report_name + '-' + uid + '.csv'
            return report_path
        else:
            return None
    else:
        if os.path.exists(root_path):
            first_path = root_path + '/' + osv_title
            if not os.path.exists(first_path):
                os.makedirs(first_path)
            report_path = first_path + '/' + report_name + '.csv'
            return report_path
        else:
            return None
