import logging
import os
import re

from oecp.utils.shell import shell_cmd
from oecp.proxy.rpm_proxy import RPMProxy
from oecp.result.compare_result import CMP_RESULT_SAME, CompareResultComposite, CMP_TYPE_RPM, CMP_TYPE_RPM_ABI, \
    CompareResultComponent, CMP_RESULT_DIFF
from oecp.executor.base import CompareExecutor


logger = logging.getLogger('oecp')


class ABICompareExecutor(CompareExecutor):

    def __init__(self, dump_a, dump_b, config):
        super(ABICompareExecutor, self).__init__(dump_a, dump_b, config)
        self.dump_a = dump_a.run()
        self.dump_b = dump_b.run()
        cache_require_key = 'extract'
        self._work_dir = self.config.get(cache_require_key, {}).get('work_dir', '/tmp/oecp')
        self.data = 'data'
        self.split_flag = '__rpm__'

    @staticmethod
    def _set_so_mapping(library_files):
        so_mapping = {}
        for library_file in library_files:
            cmd = f'objdump -p {library_file}'
            ret, out, err = shell_cmd(cmd.split())
            if not ret and out:
                match = re.search(r'SONAME\s(.+)\s', out)
                if match:
                    so_name = match.groups()[0].strip()
                    so_mapping.setdefault(so_name, library_file)
        return so_mapping

    def _get_library_pairs(self, files_a, files_b):
        library_pairs = []
        so_mapping_a = self._set_so_mapping(files_a)
        so_mapping_b = self._set_so_mapping(files_b)
        for so_name in so_mapping_a:
            if so_name in so_mapping_b:
                library_pairs.append([so_mapping_a[so_name], so_mapping_b[so_name]])
        return library_pairs

    @staticmethod
    def _save_result(file_path, content):
        with open(file_path, "w") as f:
            f.write(content)

    def _compare_result(self, dump_a, dump_b, single_result=CMP_RESULT_SAME):
        kind = dump_a['kind']
        rpm_a, rpm_b = dump_a['rpm'], dump_b['rpm']
        result = CompareResultComposite(CMP_TYPE_RPM, single_result, rpm_a, rpm_b, dump_a['category'])
        debuginfo_rpm_path_a, debuginfo_rpm_path_b = dump_a['debuginfo_extract_path'], dump_b['debuginfo_extract_path']

        dump_a_files, dump_b_files = dump_a[self.data], dump_b[self.data]
        library_pairs = self._get_library_pairs(dump_a_files, dump_b_files)
        if not library_pairs:
            return result
        debuginfo_rpm_path_a = os.path.join(debuginfo_rpm_path_a, 'usr/lib/debug') if debuginfo_rpm_path_a else ''
        debuginfo_rpm_path_b = os.path.join(debuginfo_rpm_path_b, 'usr/lib/debug') if debuginfo_rpm_path_b else ''

        verbose_cmp_path = f'{rpm_a}__cmp__{rpm_b}'
        base_dir = os.path.join(self._work_dir, kind, verbose_cmp_path)
        for pair in library_pairs:
            base_a = os.path.basename(pair[0])
            base_b = os.path.basename(pair[1])
            cmd = "abidiff {} {} --d1 {} --d2 {}".format(
                pair[0], pair[1], debuginfo_rpm_path_a, debuginfo_rpm_path_b)

            logger.debug(cmd)
            ret, out, err = shell_cmd(cmd.split())
            if ret == 0:
                logger.debug("check abi same")
                data = CompareResultComponent(CMP_TYPE_RPM_ABI, CMP_RESULT_SAME, base_a, base_b)
                result.add_component(data)
            else:
                if not os.path.exists(base_dir):
                    os.makedirs(base_dir)
                file_path = os.path.join(base_dir, f'{base_a}__cmp__{base_b}.md')
                self._save_result(file_path, out)
                logger.debug("check abi diff")
                data = CompareResultComponent(CMP_TYPE_RPM_ABI, CMP_RESULT_DIFF, base_a, base_b, file_path)
                result.set_cmp_result(CMP_RESULT_DIFF)
                result.add_component(data)

        return result

    def compare(self):
        compare_list = []
        for dump_a in self.dump_a:
            for dump_b in self.dump_b:
                # 取rpm name 相同进行比较
                if RPMProxy.rpm_name(dump_a['rpm']) == RPMProxy.rpm_name(dump_b['rpm']):
                    result = self._compare_result(dump_a, dump_b)
                    logger.debug(result)
                    compare_list.append(result)
        return compare_list

    def run(self):
        result = self.compare()
        return result
