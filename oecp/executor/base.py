from abc import ABC, abstractmethod
from oecp.result.compare_result import CMP_RESULT_MORE, CMP_RESULT_LESS, CMP_RESULT_SAME


# 两者category指定的级别不同或者未指定
CPM_CATEGORY_DIFF = 4


class CompareExecutor(ABC):

    def __init__(self, dump_a, dump_b, config):
        self.dump_a = dump_a
        self.dump_b = dump_b
        self.config = config

    @staticmethod
    def format_dump(data_a, data_b):
        dump_set_a, dump_set_b = set(data_a), set(data_b)
        common_dump = dump_set_a & dump_set_b
        only_dump_a = dump_set_a - dump_set_b
        only_dump_b = dump_set_b - dump_set_a
        all_dump = [
            [[x, x, CMP_RESULT_SAME] for x in common_dump],
            [[x, '', CMP_RESULT_MORE] for x in only_dump_a],
            [['', x, CMP_RESULT_LESS] for x in only_dump_b]
        ]
        return all_dump

    @staticmethod
    def format_dump_kabi(data_a, data_b):
        list_a = list(data_a)
        list_b = list(data_b)
        h_a = {}
        h_b = {}
        same = []
        changed = []
        losted = []
        all_dump = []

        for a in list_a:
            t = a.split(" = ")
            h_a[t[0]] = t[0] + " " + t[1]

        for b in list_b:
            t = b.split(" = ")
            h_b[t[0]] = t[0] + " " + t[1]

        for k, va in h_a.items():
            vb = h_b.get(k, None)
            if vb == None:
                losted.append([va, '', 'losted'])
            elif va == vb:
                same.append([va, vb, 'same'])
            else:
                changed.append([va, vb, 'changed'])

        # all_dump.append(same).append(changed).append(losted)
        all_dump.append(same)
        all_dump.append(changed)
        all_dump.append(losted)
        return all_dump

    @abstractmethod
    def run(self):
        pass
