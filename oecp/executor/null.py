from oecp.executor.base import CompareExecutor


class NullExecutor(CompareExecutor):

    def __init__(self, dump_a, dump_b, config=None):
        super(NullExecutor, self).__init__(dump_a, dump_b, config)
        if hasattr(dump_a, 'run') and hasattr(dump_b, 'run'):
            dump_a.run()
            dump_b.run()

    def run(self):
        return []
